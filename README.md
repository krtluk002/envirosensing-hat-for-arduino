# Arduino Envirosensing HAT

A repo that demonstrates a useful git repo configuration for archiving a KiCAD project for the Arduino Envirosensing HAT.

This README file describes how to use the repository, how the repo is structured, and how the whole project comes together.

# How to use this repo
1. Clone it recursively:
```bash
git clone https://gitlab.com/krtluk002/envirosensing-hat-for-arduino.git --recursive
```

3. Keep an eye on this repo, and pull down updates as new content is added and/or explained
# What is on here?
This repo includes folders consisting of:
* Simulations - The simulations used for the design of the HAT for component values and component selection.
* PCB - Consists of the KiCAD files
* Firmware - Consists of the code to be written to the microcontroller.
* CAD - Consists of the 3D modelling of the PCB and hardcasing for the HAT for potential 3D printing.
* Docs - Documentation of the various design stages, and technical reports describing how the modules work and how to use them. Also includes the datasheets of the components used in the HAT.
* Production - A summary of the potential PCB manufacturers, BOM files and Gerber files.

(More information about these folders is available in their READMEs)

# Who would use this repository?
This repo is designed to lay out the design process of the HAT and allow the average user to learn how the HAT works when they want to implement it into their system.

# How to install the contents on this repository?
(Under development)
# How to use the contents within this repository?
(Under development)
# How to get help with using the contents?
(Under development)
# What is the future of this repository?
(Under development)
# Current status of this repository?
The repo is currently in alpha development.
# How to contribute towards this repository?
(Under development)
# Licensing conditions
(Under development)

